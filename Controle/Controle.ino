/*
  Présentation du programme
    Programme de contrôle des moteurs
    par l'équipe EasyCourses - Pierre Lagourgue - 2015
    créé par Stéphane LALLEMAND et Paul DEVEAUX
  Notes du constructeur
    TX is motor S1, RX is motor S2
    Speed from -127 to 127 (0 is stop)
  Notes des programmeurs :
    Les moteurs doivent etre branchés pareillement !
    Carte moteur en mode Simplified Serial (136 up / 245 down)
*/
#include <SoftwareSerial.h>
#include <SabertoothSimplified.h>

double goValue = 0;
// Pins des entrées du joystick matériel
int go = (A1); // Avant / arrière (bleu)
int side = (A2); // Droite / Gauche (jaune)
int pot = (A0); // Vitesse (vert)
int sus;
int sx;
int CBO;
int PD;

boolean connecte = false;

// Objets de contrôle des moteurs
SoftwareSerial SWSerial(10, 11); // RX on pin 10 (to S2), TX on pin 11 (to S1).
SabertoothSimplified ST(SWSerial); // Use SWSerial as the serial port.



void setup()
{
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  SWSerial.begin(38400);
  Serial.begin(9600);
  pinMode(go, INPUT);
  pinMode(side, INPUT);
  pinMode(pot, INPUT);
}

void loop()
{
  if ((digitalRead(2) || digitalRead(3) || digitalRead(4)))
  {
    connecte = true;
  }

  if (!connecte)
  {
    // Partie contrôle joystick matériel
    if (analogRead(pot) < 410)
    {
      sus = map(analogRead(go), 795, 170, 127, -127);
      sx = map(analogRead(side), 170, 865, -127, 127);

      //  Serial.println(sus);
      // Serial.println(sx);

      if (sus < 20 && sus > -20 && sx < 5 && sx > -5)
      {
        ST.motor(1, 0);
        ST.motor(2, 0);
      }

      if (sus < -20 || sus > 20)
      {
        ST.motor(1, sus);
        ST.motor(2, sus);
      }

      if (sus > -20 && sus < 20)
      {
        if (sx > 5)
        {
          ST.motor(1, -sx);
          ST.motor(2, 0);
        }
        if (sx < -5)
        {
          ST.motor(1, 0);
          ST.motor(2, sx);
        }
      }
    }
    if (analogRead(pot) > 411)
    {
      sus = map(analogRead(go), 795, 170, 90, -90);
      sx = map(analogRead(side), 170, 865, -90, 90);

      if (sus < 20 && sus > -20 && sx < 5 && sx > -5)
      {
        ST.motor(1, 0);
        ST.motor(2, 0);
      }

      if (sus < -20 || sus > 20)
      {
        ST.motor(1, sus);
        ST.motor(2, sus);
      }

      if (sus > -20 && sus < 20)
      {
        if (sx > 5)
        {
          ST.motor(1, -sx);
          ST.motor(2, 0);
        }
        if (sx < -5)
        {
          ST.motor(1, 0);
          ST.motor(2, sx);
        }
      }

    }

  }
  else
  {
    //Partie contrôle à distance
    int p2 = digitalRead(2);
    int p3 = digitalRead(3);
    int p4 = digitalRead(4);

    if (p2 && p3 && !p4) {
      Serial.println("Avancer");
      ST.motor(1, 90);
      ST.motor(2, 90);
    } else if (p2 && !p3 && !p4) {
      Serial.println("A droite");
      ST.motor(1, 0);
      ST.motor(2, 90);
    } else if (p2 && p3 && p4) {
      Serial.println("Reculer");
      ST.motor(1, -90);
      ST.motor(2, -90);
    } else if (p2 && !p3 && p4) {
      Serial.println("A gauche");
      ST.motor(1, 90);
      ST.motor(2, 0);
    } else if (!p2 && !p3 && p4) {
      Serial.println("STOP");
      ST.motor(1, 0);
      ST.motor(2, 0);
    } else if (!p2 && !p3 && !p4) {
      Serial.println("DECONNECTE");
      connecte = false;
    }
  }
  delay(50);
}

